package model

import "time"

type Default struct {
	ID        int64     `json:"id,omitempty"`
	CreatedAt time.Time `sql:"default:now()"`
	DeletedAt time.Time `pg:",soft_delete"`
	UpdatedAt time.Time
}
