package model

import (
	"fmt"
	"strconv"

	"github.com/graphql-go/graphql"
	"gitlab.com/aside.hazle/golang-graphql-postgre-todo/modules"
)

type Todo struct {
	tableName struct{} `sql:"todo" json"omitempty"`
	Default
	Todo     string `json:"todo"`
	Priority int64  `json:"priority"`
}

func (*Todo) GrapqlType() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "Todo",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"todo": &graphql.Field{
				Type: graphql.String,
			},
			"priority": &graphql.Field{
				Type: graphql.String,
			},
		},
	})
}

func (*Todo) Query() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"todos": &graphql.Field{
				Type: graphql.NewList((*Todo)(nil).GrapqlType()),
				Args: graphql.FieldConfigArgument{
					"priority": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					priority := p.Args["priority"].(string)
					// select
					var todo []Todo

					db, _ := modules.OpenDB(true)
					defer db.Close()

					err := db.Model(&todo).Where("priority = ?", priority).Select()
					fmt.Println(todo)
					if err != nil {
						fmt.Println(err)
					}

					// struct to json
					return todo, nil
				},
			},
		},
	})
}

func (*Todo) Mutation() *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"createTodo": &graphql.Field{
				Type: (*Todo)(nil).GrapqlType(),
				Args: graphql.FieldConfigArgument{
					"todo": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"priority": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					todo := new(Todo)
					todo.Todo = p.Args["todo"].(string)
					todo.Priority, _ = strconv.ParseInt(p.Args["priority"].(string), 10, 64)

					// insert
					db, _ := modules.OpenDB(true)
					err := db.Insert(todo)
					if err != nil {
						fmt.Println(err)
					}
					return nil, nil
				},
			},
		},
	})
}

func (*Todo) Schema() graphql.Schema {
	schema, _ := graphql.NewSchema(graphql.SchemaConfig{
		Query:    (*Todo)(nil).Query(),
		Mutation: (*Todo)(nil).Mutation(),
	})
	return schema
}
