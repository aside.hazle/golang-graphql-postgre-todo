package modules

import (
	"fmt"
	conf "gitlab.com/aside.hazle/golang-graphql-postgre-todo/config"
	"log"
	"time"

	"github.com/go-pg/pg"
)

// Load Config.toml
var config = conf.LoadConfig(".")

// database connection
func OpenDB(logger bool) (*pg.DB, error) {
	log.Println("Database is connecting...")
	db := pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%s", config.DBHost, config.DBPort),
		User:     config.DBUser,
		Password: config.DBPassword,
		Database: config.DBName,
	})

	if db == nil {
		log.Println("Retry database connection in 5 seconds...")
		time.Sleep(time.Duration(5) * time.Second)
		return OpenDB(true)
	}

	log.Println("Database is connected!")

	// Logger
	if (logger) {
		db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			if err != nil {
				panic(err)
			}

			log.Printf("%s %s", time.Since(event.StartTime), query)
		})
	}
	return db, nil
}
