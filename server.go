package main

import (
	"encoding/json"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/graphql-go/graphql"
	"gitlab.com/aside.hazle/golang-graphql-postgre-todo/model"
	"gitlab.com/aside.hazle/golang-graphql-postgre-todo/modules"
	"log"
	"net/http"
)

func main() {
	// Database Connection
	db, err := modules.OpenDB(false)
	defer db.Close()

	// Create schema
	err = createSchema(db, true)
	if err != nil {
		log.Fatalf("Dose not created schema: %s \n", err)
	}

	http.HandleFunc("/graphql", func(w http.ResponseWriter, r *http.Request) {
		result := graphql.Do(graphql.Params{
			Schema:        (*model.Todo)(nil).Schema(),
			RequestString: r.URL.Query().Get("query"),
		})
		json.NewEncoder(w).Encode(result)
	})
	http.ListenAndServe(":3000", nil)
}

func createSchema(db *pg.DB, force bool) error {
	models := []interface{}{
		&model.Todo{},
	}

	// Migration
	for _, model := range models {

		// Drop table
		if force {
			err := db.DropTable(model, &orm.DropTableOptions{
				IfExists: true,
			})
			errCheck(err)
		}

		// Create table
		err := db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
		})
		errCheck(err)

	}

	return nil
}


func errCheck(err error) {
	if err != nil {
		panic(err)
	}
}
