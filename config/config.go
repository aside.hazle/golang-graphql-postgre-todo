package config

import (
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	AppName string

	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string
	DBName     string
}

func LoadConfig(path string) *Config {
	config := viper.New()
	config.SetConfigName("Config")
	config.AddConfigPath(".")
	err := config.ReadInConfig()
	if err != nil {
		log.Fatalf("Fatal error context file: %s \n", err)
	}

	return &Config{
		AppName: config.Get("app-name").(string),

		DBHost: config.Get("db.host").(string),
		DBPort: config.Get("db.port").(string),
		DBUser: config.Get("db.user").(string),
		DBPassword: config.Get("db.password").(string),
		DBName: config.Get("db.dbname").(string),
	}
}
